require('./renderer')
const {
  globalShortcut,
  ipcRenderer
} = require('electron').remote

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import VueProgressBar from 'vue-progressbar'
import './plugins/ant-design-vue'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

globalShortcut.register('escape', () => {
  console.log('Esc: voltar ao inicio')
  router.push({
    name: 'Painel'
  })
})

Vue.use(PerfectScrollbar)
Vue.use(VueProgressBar, {
  color: '#409EFF',
  failedColor: '#ff4949',
  thickness: '8px',
  height: '15px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300,
  },
  autoRevert: true,
  location: 'bottom',
  inverse: false,
})


Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
