import Vue from 'vue'
import Antd from 'ant-design-vue'
import {
  message,
  notification
} from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';

Vue.use(Antd)

message.config({
  top: '50px',
  maxCount: 3
})

notification.config({
  placement: 'bottomLeft',
  duration: 3
})
