const log = require('electron-log')
const cryto = require('crypto')
const secret = 'a661f332e071ad193ed6bea30f418a8d'

export function encrypt(value) {
  if (value == undefined || value == null) {
    log.info('Não é possivel ')
    return
  } else {
    const iv = Buffer.from(cryto.randomBytes(16))
    const cipher = cryto.createCipheriv('aes-256-cbc', Buffer.from(secret), iv)
    let encrypted = cipher.update(value)
    encrypted = Buffer.concat([encrypted, cipher.final()])
    return `${iv.toString('hex')}:${encrypted.toString('hex')}`
  }
}

export function decrypt(value) {
  if (value == undefined || value == null) {
    log.info('Não é possivel ')
    return
  } else {
    const [iv, encrypted] = value.split(':')
    const ivBuffer = Buffer.from(iv, 'hex')
    const decipher = cryto.createDecipheriv('aes-256-cbc', Buffer.from(secret), ivBuffer)
    let content = decipher.update(Buffer.from(encrypted, 'hex'))
    content = Buffer.concat([content, decipher.final()])
    return content.toString()
  }
}
