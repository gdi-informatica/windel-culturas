const customTitlebar = require('custom-electron-titlebar')
const pack = require('../package.json')
let titlebar = new customTitlebar.Titlebar({
  backgroundColor: customTitlebar.Color.fromHex('#23232e'),
  overflow: 'hidden',
  maximizable: false,
  icon: 'icons/win/icon.ico',
})

document.title = `Versão ${pack['version']}`
titlebar.updateTitle()
