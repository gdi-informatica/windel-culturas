import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
import { createPersistedState, createSharedMutations } from "vuex-electron"

Vue.use(Vuex)

const store = new Vuex.Store({
  namespaced: true,
  modules,
  plugins: [createPersistedState()],
  
  strict: process.env.NODE_ENV !== "production"
})

export default store
