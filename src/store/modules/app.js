export default {
    namespaced: true,
    state: {
      openWithWindows: true,
      alwaysOnTop: true
    },
    mutations: {
      SET_APP_MODE(state, mode) {
        state.darkMode = mode
      },
      SET_APP_INITIALIZE(state, openWithWindows) {
        state.openWithWindows = openWithWindows
      },
      SET_APP_ALWAYS_ON_TOP(state, alwaysOnTop) {
        state.alwaysOnTop = alwaysOnTop
      },
    },
    getters: {
      getTheme: state => {
        return state.darkMode
      },
      getAlwaysOnTop: state => {
        return state.alwaysOnTop
      }
    }
  }
  